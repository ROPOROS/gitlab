'use strict';

exports.config = {
  app_name: ['UWAAS_MAILING_APM'],
  license_key: '1a48b602f88198ed3a68dc38e740b94FFFRNRAL',
  
  error_collector: {
    enabled: true
  },

   browser_monitoring: {
    enabled: true,
    error_collector: {
      enabled: true
    },
    transaction_tracer: {
      enabled: true
    },
    logging: {
      level: 'info'
    }
  },
  
  transaction_tracer: {
    enabled: true
  },
  
  application_logging: {
    forwarding: {
      enabled: true
    }
  },
  
  logging: {
    level: 'info'
  },
  
  allow_all_headers: true,
  
  attributes: {
    /**
     * Prefix of attributes to exclude from all destinations. Allows * as wildcard
     */
  },
  
  // Enable Browser Monitoring
 
};
